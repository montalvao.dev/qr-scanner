sap.ui.define([
	"./BaseController",
	"sap/ui/model/json/JSONModel",
	"../model/utils"
], function (BaseController, JSONModel, utils) {
	"use strict";

	return BaseController.extend("qr.scanner.controller.App", {

		onInit: function () {
			///
			/// CONEXÃO DEFAULT COM SAP ODATA
			///
			/*
			var that = this;
			var sUrl = document.location.hostname;
			var sEnviroment = sUrl.includes("launchpad") ? "PRD" : "QAS";
			var sServiceUrl = sEnviroment === "PRD" ?
				"http://launchpad.uniaoquimica.com.br/Portal/webapp/model/destination.json" :
				"http://sqap00.uniaoquimica.com.br:9094/Portal/webapp/model/destination.json";

			fetch(sServiceUrl)
				.then(function (r) {
					r.json().then(function (oService) {
						this._oService = oService.SOCREATE[sEnviroment];

						var oModel = new sap.ui.model.odata.v2.ODataModel(
							this._oService.URL + "?sap-client=" + this._oService.CLIENT,
							{
								headers: {
									"Authorization": "Basic " + btoa(atob(this._oService.USER) + ":" + atob(this._oService.PASS)),
									"X-Requested-With": "X"
								},
								withCredentials: true,
								maxDataServiceVersion: "2.0",
								json: false,
								defaultUpdateMethod: "Put",
								defaultCountMode: "None",
								useBatch: false
							}
						);

						this.getOwnerComponent().setModel(oModel);

						var fnSetAppNotBusy = function () {
							that.setBusy(false);
						};

						this.getOwnerComponent().getModel().metadataLoaded()
							.then(fnSetAppNotBusy)
							.catch(fnSetAppNotBusy);

						this.getOwnerComponent().getModel().attachMetadataFailed(function (oError) {
							var sMessage;
							var sText = oError.getParameter("responseText");
							if (sText != "") {
								sMessage = oError.getParameter("responseText");
							} else {
								sMessage = this.getResourceBundle().getText("DEFAULT_SERVER_ERROR");
							}

							utils.dialogErrorMessage(oError, sMessage);
							fnSetAppNotBusy();
						}.bind(this));

						return this._oService;
					}.bind(this));
				}.bind(this));

			
			// since then() has no "reject"-path attach to the MetadataFailed-Event to disable the busy indicator in case of an error
			*/

			var oViewModel = new JSONModel({
				busy: true,
				delay: 0,
				layout: "OneColumn",
				previousLayout: "",
				actionButtonsInfo: {
					midColumn: {
						fullScreen: false
					}
				}
			});

			this.setModel(oViewModel, "appView");

			// apply content density mode to root view
			this.getView().addStyleClass(
				this.getOwnerComponent().getContentDensityClass()
			);
		}

	});

});