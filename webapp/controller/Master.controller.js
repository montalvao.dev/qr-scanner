sap.ui.define([
	"./BaseController",
	"../model/formatter"
], function (BaseController, formatter) {
	"use strict";


	return BaseController.extend("qr.scanner.controller.Master", {

		formatter: formatter,

		onInit: function () {
			this.getRouter().getRoute("master").attachPatternMatched(this._onMasterMatched, this);
		},

		openBarcodeScannerDialog: function (oEvent) {
			this.byId("bsd").show();
		},

		onScanError: function (oEvent, oAnother) {
			console.log(oEvent);
			console.log(oAnother);

			this.byId("scanResultLabel").setText("Error result: " + oEvent.getParameter("message"));
			this.byId("bsd").close();
		},

		onScanSuccess: function (oEvent, oAnother) {
			console.log(oEvent);
			console.log(oAnother);

			this.byId("scanResultLabel").setText("Success result: " + oEvent.getParameter("text"));
			this.byId("bsd").close();
		},

		_onMasterMatched: function () {
			this.setLayout("OneColumn");
			this.setBusy(false);

			var html5QrcodeScanner = new Html5QrcodeScanner(
				"scan", { fps: 10, qrbox: 250 });

			var onScanSuccess = function (decodedText, decodedResult) {
				// Handle on success condition with the decoded text or result.
				console.log(`Scan result: ${decodedText}`, decodedResult);
				this.byId("scanResultLabel").setText("Success result: " + decodedText);

				html5QrcodeScanner.clear();
			}.bind(this);

			html5QrcodeScanner.render(onScanSuccess);
		},

	});

});